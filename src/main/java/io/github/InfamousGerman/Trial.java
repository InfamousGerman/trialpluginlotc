package io.github.InfamousGerman;

import de.diddiz.LogBlock.Actor;
import de.diddiz.LogBlock.Consumer;
import de.diddiz.LogBlock.LogBlock;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.enchantments.*;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Graham on 8/27/2016.
 */
public class Trial extends JavaPlugin implements Listener { // Overarching Class implements a listener for events
    private FileConfiguration config; // Assigning config instance to a variable
    private static Trial plugin; // Creating plugin instance
    private Consumer lbconsumer; // Creating a variable for LB consumer


    public static List<Player> TramplePlayers; //Creating list that stores players who are allowed to trample crops

    public void onEnable() { // Method that fires when enabled
        TramplePlayers = new ArrayList<Player>(); // Making TramplePlayers an arraylist to add players
        Bukkit.getServer().getPluginManager().registerEvents(this, this); // Registering events
        saveConfig(); // Creating config file
        this.config = getConfig(); // Assigning config to a private viarable
        Trial.plugin = this; // Assigning plugin instance to a variable
        List<String> tramplePlayersNames = (List<String>) config.get("List.Trample.Players"); // Getting list of players who can trample crops
        if (tramplePlayersNames != null && tramplePlayersNames.size() > 0) { // Checking if the list is not empty
            for (String name : tramplePlayersNames) { // Looping through player names
                Player p = Bukkit.getPlayer(name); // Assigning p variable to a playername
                if (p != null) { // Checking if variable p is null
                    TramplePlayers.add(p); // Adding player name to the TramplePlayers list
                }
            }
        }
        PluginManager pm = getServer().getPluginManager(); // Getting plugin manager
        Plugin plugin = pm.getPlugin("LogBlock"); // Getting logblock
        if (plugin != null) { // Checking that the plugin load
            lbconsumer = ((LogBlock) plugin).getConsumer(); // Assigning consumer logblock variable
        }
        this.getCommand("farmtrample").setExecutor(new Commands()); // Registering commands class
    }

    public void onDisable() { // Method to be run when server is disabled
        List<String> tramplePlayersNames = new ArrayList<String>(); // Storing tramplePlayerNames as an arraylist so I can add players to the list
        for (Player p : TramplePlayers) { // Looping through TramplePlayers list
            tramplePlayersNames.add(p.getName()); // Adding player names to the list
        }
        config.set("List.Trample.Players", null); // Clears list from config
        saveConfig(); // Saves config
        config.set("List.Trample.Players", tramplePlayersNames); // Sets list to list of new players
        saveConfig(); // Saves config

    }

    @EventHandler// Calls the event handler
    public void onBlockBreak(BlockBreakEvent e) { // Class specifies what event I am using
        Block b = e.getBlock(); // Defining variable b as a block
        Player p = e.getPlayer(); // Defining variable p as a player gotten from usernames

        int randomDrop = (int) (Math.random() * (0 + 100) + 1) + 0; // Generates a number that is to be used later for random loot
        e.getBlock().getDrops().clear(); // Clearing default drops of crops
        if (b.getType() == Material.CROPS || b.getType() == Material.NETHER_WARTS || b.getType() == Material.CARROT) { // Checking if block broken is Wheat, Nether wart or carrots
            ItemStack item = p.getEquipment().getItemInMainHand(); //Get the item in players hand

            if (p.getEquipment().getItemInMainHand().getType() == Material.WOOD_HOE || p.getEquipment().getItemInMainHand().getType() == Material.STONE_HOE) { // Checking if the item held is a hoe
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.SEEDS, 1)); // Drops 1 seed
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(b.getType(), 1)); //Drop 1 of the crop type broken
                p.getEquipment().getItemInMainHand().setDurability((short)(item.getDurability() + 1)); // Subtracts 1 durability when the block is broken
                if (randomDrop == 100) { // Checks if the random integer that was generated is 100
                    e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.IRON_INGOT, 1)); // Drops 1 Iron ingot if the integer is 100

                }
            }

            if (p.getEquipment().getItemInMainHand().getType() == Material.IRON_HOE || p.getEquipment().getItemInMainHand().getType() == Material.GOLD_HOE) { // Checks if the tool is an Iron hoe or gold hoe
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.SEEDS, 2)); // Drops 2 seeds
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(b.getType(), 2)); //Drop 2 of the crop type broken
                p.getEquipment().getItemInMainHand().setDurability((short) (item.getDurability() + 1)); // Subtracts 1 durability when the block is broken
                if (randomDrop == 95 - 100) { // Checks if the random integer generated is from 95-100
                    e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.GOLD_INGOT, 2)); // Drops 2 gold ingots if the integer is from 95-100
                }
            }

            if (p.getEquipment().getItemInMainHand().getType() == Material.DIAMOND_HOE) { // Checks if the tool used is a diamond hoe
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.SEEDS, 3)); // Drops 3 seeds
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(b.getType(), 3)); //Drop 3 of the crop type broken
                p.getEquipment().getItemInMainHand().setDurability((short)(item.getDurability() + 1)); // Subtracts 1 durability from the tool when it breaks a block
                if (randomDrop == 90 - 100) { // Checks if the random integer generated is from 90-100
                    e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.DIAMOND, 3)); // Drops 3 diamonds
                }
            }

            int level = p.getEquipment().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS); // Defining the new integer 'level' as the fortune enchant
            if (level == 1) { // Checking if the fortune level is 1
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.SEEDS, 1)); // Drops 1 seed
            }
            if (level == 2) { // Checking if the fortune level is 2
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.SEEDS, 2)); // Drops 2 seeds
            }
            if (level == 3) { // Checking if the fortune level is 3
                e.getBlock().getWorld().dropItem(b.getLocation(), new ItemStack(Material.SEEDS, 3)); // Drops 3 seeds
            }

            if (item.getDurability() >= item.getType().getMaxDurability()) { // Checking if the durability is up
                p.getInventory().remove(p.getEquipment().getItemInMainHand()); // Removes item
                p.updateInventory(); // Updates player Inventory
            }

            lbconsumer.queueBlockBreak(Actor.actorFromEntity(p),e.getBlock().getState()); // Logblocking the block broken

        }


    }


    @EventHandler// Calls Eventhandler
    public void onPlayerInteractEvent(PlayerInteractEvent e) { // Specifies the type of event that I'm working with
        if (e.getAction().equals(Action.PHYSICAL)) { // Checking the action
            if (e.getClickedBlock().getType() == Material.SOIL) { // Checking if the block is soil
                Block crop = e.getClickedBlock().getWorld().getBlockAt(e.getClickedBlock().getLocation().add(0, 1, 0)); // Get block above the soil
                if (crop.getType() == Material.WHEAT || crop.getType() == Material.NETHER_WARTS || crop.getType() == Material.CARROT) { // Check if the blocks are crops
                    if (Trial.TramplePlayers.contains(e.getPlayer())) { // Check if player is allowed to trample crops
                        return; //if true, do nothing
                    } else {
                        e.setCancelled(true); // If false, cancel the crops from being trampled
                    }

                }

            }
        }

    }

    @EventHandler// Calls eventhandler
    public void onPistonMove(BlockPistonExtendEvent e) { // Specifies that Im working with the BlockPistonExtend event
        for (Block b : e.getBlocks()) { // Loops through the b variable
            if (b.getType() == Material.CROPS || b.getType() == Material.NETHER_WARTS || b.getType() == Material.CARROT) { // Checks if the block that the piston extended on is Wheat, Nether warts or carrots
                b.getWorld().createExplosion(b.getLocation(), 4.0f, false);  // Creates an explosion
                for (Player p : b.getWorld().getPlayers()) { // Gets all players in the world
                    if (p.getLocation().distance(b.getLocation()) < 6) { // Checks if the distance away from the origin of the explosion is less than 6 block
                        p.sendMessage(ChatColor.RED + "You tried to circumvent the farming plugin therefore we killed your farm!"); // Displays message
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPistonMove(BlockPistonRetractEvent e) { // Specifies I'm working with a piston retract event
        for (Block b : e.getBlocks()) { // Loops through the b variable to get block
            if (b.getType() == Material.CROPS || b.getType() == Material.NETHER_WARTS || b.getType() == Material.CARROT) { // Checks if block is Wheat, Nether warts or carrots
                b.getWorld().createExplosion(b.getLocation(), 4.0f, false); // Creates an explosion where the piston retracts
                for (Player p : b.getWorld().getPlayers()) { // Loops through all players in the world
                    if (p.getLocation().distance(b.getLocation()) < 6) { // Checks if distance away from the explosion is less than 6
                        p.sendMessage(ChatColor.RED + "You tried to circumvent the farming plugin therefore we killed your farm!"); // Displays Message
                    }
                }
            }
        }
    }

    public static Trial getPlugin() {
        return Trial.plugin; //returns plugin
    }
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) { // Specifies I'm working with the PlayerQuitEvent
        if (TramplePlayers.contains(e.getPlayer())) { // Checking if the list of players allowed to trample crops contains the player from the player quit event
            TramplePlayers.remove(e.getPlayer()); // Removing player from the list to prevent memory leak
        }
    }
}



















