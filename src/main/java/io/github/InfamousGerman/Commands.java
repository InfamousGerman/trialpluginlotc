package io.github.InfamousGerman;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


/**
 * Created by Graham on 9/4/2016.
 */
public class Commands implements CommandExecutor { // Overarching class allows Command options to be implemented

    public boolean onCommand(CommandSender player, org.bukkit.command.Command FarmingImproved, String label, String[] args) {
        if(player instanceof Player){ //check if commandsender is a player
            if(FarmingImproved.getName().equals("farmtrample")){ //check what the player typed
                if(args.length == 0) { //check if player did not use an argument
                    if (Trial.TramplePlayers.contains(player)) { //check if player is allowed to trample crops
                        Trial.TramplePlayers.remove(player); //disallow them from trampling crops if they were allowed to
                        player.sendMessage("You can no longer trample crops!");
                    } else {
                        Trial.TramplePlayers.add((Player) player); //allow them to trample crops if they werent allowed to
                        player.sendMessage("You can now trample crops!");
                    }
                }else if(args.length == 1){ //check if player used an argument
                    if(player.hasPermission("FarmingImproved.flagrate")){ //check if player has the mod permissions
                        String name = args[0]; //get the name from the argument
                        if (Trial.TramplePlayers.contains(Bukkit.getPlayer(name))) { //check if player is allowed to trample crops
                            Trial.TramplePlayers.remove(player); //disallow them from trampling crops if they were allowed to
                            player.sendMessage(name + " can no longer trample crops!");
                        } else {
                            Trial.TramplePlayers.add(Bukkit.getPlayer(name)); //allow them to trample crops if they werent allowed to
                            player.sendMessage(name + " can now trample crops!");
                        }
                    }else{
                        player.sendMessage("no permission");
                    }
                }


            }
        }
        return false;
    }
}


